Napisz korzystając z metodologii TDD program który:

Wywołany z konsoli z parametrem kodu waluty, poda jej obecny kurs oraz moment jej podania.

Dane konkretnej waluty pobierz z:
http://api.nbp.pl/api/exchangerates/rates/A/USD?format=json

Dane jakie waluty są dostępne pobierz z
http://api.nbp.pl/api/exchangerates/tables/A?format=json

1. Program po wywpołaniu:

`php rates.php -rate USD`

Powinien zwrócić następujący wpis:

- 3,39 PLN; on 2018-03-28

W przypadku błędnego kodu waluty powinien zadziałać następująco:

- Rate for XYZ is not available

Przy wywołaniu:

 `php rates.php -rate USD 100`
 
Powinien zwrócić wpis:

- For 100 USD you will pay 339 PLN

Przy wywołaniu: 

 `php rates.php -all-rates`
 
program powinien zwrócić listę dostępnych kodów walut, wraz z ich nazwami np:

- USD (dolar amerykański)
- EUR (euro)
- CHF (frank szwajcarski)

Przy wywołaniu bez parametru albo z parametrem `-h` czy `--help`

Powinien zwrócić treść pomocy o dostępnych komendach

Program powinien zawierać:

* Controller 
* Obiekty komend dla -rate -all-rates oraz -h
* Serwis pobierający określone dane
* Serwis odpowiadający za wydruki
* Serwis wykonujący obliczenia

Do połączenia się i pobrania dancyh możesz wykorzystać gotową bibliotekę załączoną w composer.json, 
albo inną ulubioną. Jeśli chcesz możesz napisać również klienta sam.
